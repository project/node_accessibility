<?php

/**
 * @file
 * Install file for node accessibility.
 */

/**
 * Implementation of hook_schema().
 */
function node_accessibility_schema() {
  $schema = [];

  $schema['node_accessibility_type_settings'] = [
    'description' => t("Stores.node type specific accessibility settings."),
    'fields' => [
      'node_type' => [
        'description' => t("The node type machine name."),
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
      ],
      'enabled' => [
        'description' => t("Whether or not quail is enabled and/or required for this node type."),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ],
      'method' => [
        'description' => t("The validation method machine name."),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      // @todo: format_content is not yet implemented but added as a future addition.
      'format_content' => [
        'description' => t("The formatting filter to apply before performing validation on the content."),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ],
      'format_results' => [
        'description' => t("The formatting filter to apply when presenting the validation results."),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ],
      'title_block' => [
        'description' => t("The title block markup tag to use when renderring the validation result titles."),
        'type' => 'varchar',
        'length' => 16,
        'not null' => TRUE,
        'default' => '',
      ],
      'standards' => [
        'description' => t("A serialized array of validation methods in use. This is intended to be encoded with PHP's json_encode()."),
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => ['node_type'],
  ];

  $schema['node_accessibility_problems'] = [
    'description' => t("Node validation problem statistics."),
    'fields' => [
      'id' => [
        'description' => t("The primary key used to represent this problem."),
        'type' => 'serial',
        'size' => 'big',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'nid' => [
        'description' => t("The node's ID from {node}.nid."),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'vid' => [
        'description' => t("The node's version ID from {node}.vid."),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'test_id' => [
        'description' => t("The numeric ID from {quail_api_tests}.id, representing a specific error."),
        'type' => 'int',
        'size' => 'big',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'test_severity' => [
        'description' => t("The numeric ID from {quail_api_tests}.severity, representing the severity of an error."),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'line' => [
        'description' => t("The line number in which the error happened."),
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'element' => [
        'description' => t("A snippet of the code that failed validation."),
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => ['id'],

    'foreign keys' => [
      'nid' => [
        'table' => 'node',
        'columns' => ['nid' => 'nid'],
      ],
      'vid' => [
        'table' => 'node_revision',
        'columns' => ['vid' => 'vid'],
      ],
      'test_id' => [
        'table' => 'quail_api_tests',
        'columns' => ['test_id' => 'id'],
      ],
    ],
  ];

  $schema['node_accessibility_stats'] = [
    'description' => t("Node validation problem statistics."),
    'fields' => [
      'nid' => [
        'description' => t("The node's ID from {node}.nid."),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'vid' => [
        'description' => t("The node's version ID from {node}.vid."),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'description' => t("A user id for the user who performed the validation."),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'timestamp' => [
        'description' => t("The date in which this validation was performed."),
        'type' => 'int',
        'size' => 'big',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
    ],
    'primary key' => ['nid', 'vid'],

    'foreign keys' => [
      'nid' => [
        'table' => 'node',
        'columns' => ['nid' => 'nid'],
      ],
      'vid' => [
        'table' => 'node_revision',
        'columns' => ['vid' => 'vid'],
      ],
      'test_id' => [
        'table' => 'quail_api_tests',
        'columns' => ['test_id' => 'id'],
      ],
      'test_severity' => [
        'table' => 'quail_api_tests',
        'columns' => ['test_severity' => 'severity'],
      ],
      'uid' => [
        'table' => 'user',
        'columns' => ['uid' => 'uid'],
      ],
    ],
  ];

  // mysql often does not support default values on text fields, so remove it.
  if (\Drupal::database()->driver() == 'mysql') {
    unset($schema['node_accessibility_type_settings']['fields']['enabled']['default']);
    unset($schema['node_accessibility_type_settings']['fields']['method']['default']);
    unset($schema['node_accessibility_type_settings']['fields']['format_content']['default']);
    unset($schema['node_accessibility_type_settings']['fields']['format_results']['default']);
    unset($schema['node_accessibility_type_settings']['fields']['title_block']['default']);
    unset($schema['node_accessibility_type_settings']['fields']['standards']['default']);

    unset($schema['node_accessibility_problems']['fields']['element']['default']);
  }

  return $schema;
}
